<?php

namespace Drupal\nftport\PluginForm;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\nft\PluginForm\MinterConfigFormBase;
use Web3\Validators\AddressValidator;

/**
 * Form class for the MinterEasyMinting plugin.

 * Provide a UX to select file URL or file upload.
 *
 * @see https://docs.nftport.xyz/docs/
 */
class NftportMinterPluginForm extends MinterConfigFormBase {

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $settings = \Drupal::service('nftport.manager')->getConfiguration();

    $plugin_id = $this->plugin->getPluginId();

    $form[$plugin_id]['account_address']['#validate'][] = '::validateAddress';
    $form[$plugin_id]['account_address']['#placeholder'] = '0x0000000000000000000000000000000000000000';

    return $form;
  }

  /**
   * File upload validation callback.
   */
  public function validateAddress(array $form, FormStateInterface $form_state) {
    if (!AddressValidator::validate($form_state->getValue('account_address'))) {
      $form_state->setErrorByName('account_address', $this->t('Invalid address.'));
    }
  }

}
