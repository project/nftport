<?php

namespace Drupal\nftport\Plugin\Minter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\nft\Plugin\Minter\MinterBase;

/**
 * Defines a minter plugin with the NFTPort Easy Minting API.
 *
 * @Minter(
 *   id = "nftport_easy_minting_file_upload",
 *   title = @Translation("NFTPort - Easy minting (file_upload)"),
 *   description = @Translation("Use Easy Minting API from NFTPort.xyz")
 * )
 */
class MinterEasyMintingFileUpload extends MinterBase {

  /**
   * {@inheritDoc}
   */
  public function mintCall($values, $configuration) {
    // @todo Get file content.
    $file_info = [
      'name' => '',
      'content' => '',
    ];
    return $this->nftPort->call('mints-easy-url', $values + [
      'chain' => $configuration['chain'] ?? 'polygon',
      'mint_to_address' => $configuration['account_address'] ?? NULL,
      'file_info' => $file_info,
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public function buildMintForm(&$form, FormStateInterface $form_state) {
    $form['file_upload'] = [
      '#type' => 'file',
      '#title' => $this->t('File'),
      '#validate' => ['::validateFileUpload'],
      '#submit' => ['::submitFileUpload'],
      '#required' => TRUE,
      '#multiple' => FALSE,
    ];
  }

  /**
   * File upload validation callback.
   */
  public function validateFileUpload(array $form, FormStateInterface $form_state) {
    $all_files = $this->getRequest()->files->get('files', []);
    if (empty($all_files['upload'])) {
      $form_state->setErrorByName('upload', $this->t('<em>Either</em> upload a file or enter a URL.'));
    }

    // Check if file exists.
    // Error if yes.
    // Set methods + populate file URL.
  }

  /**
   * File upload submit callback.
   */
  public function submitFileUpload(array $form, FormStateInterface $form_state) {
    // Save file to IPFS.
    $validators = ['file_validate_extensions' => ['opml xml']];
    if ($file = file_save_upload('upload', $validators, FALSE, 0)) {
      $data = file_get_contents($file->getFileUri());
    }
  }

}
