<?php

namespace Drupal\nftport\Plugin\Minter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\nftport\Plugin\Minter\MinterEasyMintingBase;

/**
 * Defines a minter plugin with the NFTPort Easy Minting API.
 *
 * @Minter(
 *   id = "nftport_easy_minting_file_url",
 *   title = @Translation("NFTPort - Easy minting (file_url)"),
 *   description = @Translation("Use Easy Minting API from NFTPort.xyz")
 * )
 */
class MinterEasyMintingFileUrl extends MinterEasyMintingBase {

  /**
   * {@inheritDoc}
   */
  public function mintCall($values, $configuration) {
    return $this->nftPort->call('mints-easy-url', $values + [
      'chain' => $configuration['chain'] ?? 'polygon',
      'mint_to_address' => $configuration['account_address'] ?? NULL,
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public function buildMintForm(&$form, FormStateInterface $form_state) {
    $form['file_url'] = [
      '#type' => 'ipfs_upload',
      '#title' => $this->t('File URL'),
      '#default_value' => NULL,
      '#multiple' => FALSE,
      '#required' => TRUE,
    ];
  }

}
