<?php

namespace Drupal\nftport;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\nftport\NFTPortManagerInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;
use Psr\Log\LoggerInterface;

/**
 * Helper to use the NFTPort.xyz API with ease.
 */
class NFTPortManager implements NFTPortManagerInterface {

  /**
   * The logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Config factory instance.
   */
  protected $configFactory;

  /**
   * API config.
   *
   * @var array
   */
  protected $configuration = [];

  /**
   * Build a new service instance.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(LoggerInterface $logger, ClientInterface $http_client, ConfigFactoryInterface $config_factory) {
    $this->logger = $logger;
    $this->httpClient = $http_client;
    $this->configFactory = $config_factory;

    // Configure with default values.
    $this->setConfiguration();
  }

  /**
   * Get API config
   *
   * @return array
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritDoc}
   */
  public function setConfiguration(array $values = []) {
    $settings = $this->configFactory->get('nftport.settings')->getRawData();

    $this->configuration = $values + [
      'key' => $settings['key'] ?? null,
      'url' => $settings['url'] ?? 'https://api.nftport.xyz',
      'version' => $settings['version'] ?? 'v0',
    ];

    return $this;
  }

  /**
   * Make requests more human-readable.
   *
   * @param string $resource
   *   A given call.
   *
   * @return \GuzzleHttp\Response
   *
   * @throws \Exception
   */
  public function call(string $resource, array $parameters = []) {
    // Check parameters.
    $required_parameters = ['chain'];
    switch ($resource) {
      case 'nfts':
        // Get a NFT info.
        $required_parameters[] = 'contract_address';
        $required_parameters[] = 'token_id';

      case 'mints':
        // Get a transaction info.
        $required_parameters[] = 'transaction_hash';
        break;

      case 'mints-easy-url':
        // Easily mint NFT.
        $required_parameters[] = 'name';
        $required_parameters[] = 'description';
        $required_parameters[] = 'mint_to_address';
        break;
    }
    foreach ($required_parameters as $key) {
      if (!isset($parameters[$key]) && !empty($parameters[$key])) {
        throw new \Exception('Missing or empty parameter:' . $key);
      }
    }

    // Create remote URL.
    $request_options = [];
    $method = 'POST';
    switch ($resource) {
      case 'nfts':
        $method = 'GET';
        $path = 'nfts/' . $parameters['contract_address'] . '/' . $parameters['token_id'];
        $request_options[RequestOptions::QUERY] = [
          'chain' => $parameters['chain'],
        ];

        break;

      case 'mints':
        $method = 'GET';
        $path = 'mints/' . $parameters['transaction_hash'];
        $request_options[RequestOptions::QUERY] = [
          'chain' => $parameters['chain'],
          'transaction_hash' => $parameters['transaction_hash'],
        ];
        break;

      case 'mints-easy-url':
        $path = 'mints/easy/urls';
        if ($file_url = $parameters['file_url'] ?? NULL) {
          $request_options[RequestOptions::BODY] = [
            'chain' => $parameters['chain'],
            'name' => $parameters['name'],
            'description' => $parameters['description'],
            'file_url' => $parameters['file_url'] ?? NULL,
            'mint_to_address' => $parameters['mint_to_address'] ?? NULL,
          ];
        }
        if ($file_info = $parameters['file_info'] ?? NULL) {
          $request_options[RequestOptions::QUERY] = [
            'chain' => $parameters['chain'],
            'name' => $parameters['name'],
            'description' => $parameters['description'],
            'mint_to_address' => $parameters['account_address'],
          ];
          $request_options[RequestOptions::MULTIPART] = [
            [
              'name' => 'upload',
              'filename' => $file_info['name'],
              'contents' => $file_info['content'],
            ],
          ];
        }
        break;
      default:
        $method = 'GET';
        $path = '';
        $request_options = [];
        break;
    }

    $remote_url = [];
    $remote_url[] = $this->configuration['url'];
    $remote_url[] = $this->configuration['version'];
    $remote_url[] = $path;

    return $this->request($method, implode('/', $remote_url), $request_options);
  }

  /**
   * {@inheritDoc}
   */
  public function request(string $method, string $url, array $request_options = []) {
    // Required headers.
    $h = RequestOptions::HEADERS;
    if (!isset($request_options[$h]['Authorization'])) {
      $request_options[$h]['Authorization'] = $this->configuration['key'] ?? NULL;
    }
    if (!isset($request_options[$h]['Content-Type'])) {
      $request_options[$h]['Content-Type'] = 'application/json';
    }

    // JSON encode body.
    $b = RequestOptions::BODY;
    if (isset($request_options[$b]) && \is_array($request_options[$b])) {
      $request_options[$b] = Json::encode($request_options[$b]);
    }

    // Request and decode response.
    $response = $this->httpClient->request($method, $url, $request_options);
    $result = Json::decode($response->getBody());
    if (!$result['response'] === 'OK') {
      throw new \Exception('Error during request on: ' . $url);
    }

    return $result;
  }

}
