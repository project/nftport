<?php

namespace Drupal\nftport\Plugin\Minter;

use Drupal\nft\Plugin\Minter\MinterBase;
use Drupal\nft\Utility\Metadata;
use Drupal\nft\Utility\Transaction;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a base class for minter plugin using NFTPort's Easy Minting API.
 */
abstract class MinterEasyMintingBase extends MinterBase {

  /**
   * The NFTPort service.
   *
   * @var \Drupal\nftport\NFTPortManagerInterface
   */
  protected $nftPort;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->nftPort = $container->get('nftport.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'account_address' => '',
    ];
  }

  /**
   * Return instance of the NFTPort manager.
   *
   * @return \Drupal\nftport\NFTPortManagerInterface
   */
  public function getNFTPortManager() {
    return $this->nftPort;
  }

  /**
   * Retrieve an NFT information.
   */
  public function retrieve(array $values = []) {
    return $this->nftPort->call('mints', [
      'chain' => $values['chain'] ?? NULL,
      'transaction_hash' => $values['transaction_hash'] ?? NULL,
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public function mint(Metadata $metadata, array $context = []): Transaction {
    $values = $metadata->getValues();
    $configuration = $this->getConfiguration();

    try {
      $result = $this->mintCall($values, $configuration);
    } catch (Error $e) {
      $message = $this->t('Error during Easy minting from file URL: ' . $e->getMessage());
    }

    return new Transaction([
      'status' => $result['status'] ?? NULL,
      'result' => $result ?? NULL,
      'message' => $message ?? NULL,
      'context' => $context ?? NULL,
      'metadata' => $metadata ?? NULL,
    ]);
  }
}
