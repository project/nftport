<?php

namespace Drupal\nftport;

/**
 * Common functions for NFTPort managers.
 */
interface NFTPortManagerInterface {

  /**
   * Make a request to the API.
   *
   * @param string $method
   *   A given method.
   * @param string $url
   *   A given resource of the API.
   * @param array $options
   *
   * @return array Decode response body.
   *
   * @see https://docs.nftport.xyz/
   */
  public function request(string $method, string $url, array $options);

  /**
   * Configure API information.
   *
   * Must includes:
   * - key: The API key.
   * - url: The API base URL.
   * - version: The API version (e.g. 'v0').
   *
   * @param array $values
   *   A given list of API information.
   */
  public function setConfiguration(array $values = []);

  /**
   * Helper function
   *
   * @return array
   *   Basically, NFTPort module settings.
   */
  public function getConfiguration();
}
