<?php

namespace Drupal\nftport\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure our custom settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'nftport.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nftport_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $config->get('key'),
      '#required' => TRUE,
    ];
    $form['url'] = [
      '#type' => 'url',
      '#title' => $this->t('API Base URL'),
      '#default_value' => $config->get('url'),
      '#required' => TRUE,
    ];
    $form['version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Version'),
      '#default_value' => $config->get('version') ?? 'v0',
      '#required' => TRUE,
    ];
    $form['account'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default account sender'),
      '#default_value' => $config->get('account') ?? NULL,
      '#placeholder' => '0x0000000000000000000000000000000000000000',
      '#required' => TRUE,
    ];
    $form['method'] = [
      '#type' => 'select',
      '#title' => $this->t('Default method'),
      '#default_value' => $config->get('method') ?? 'file_url',
      '#required' => TRUE,
      '#options' => [
        'file_url' => $this->t('File URL'),
        'file_upload' => $this->t('File upload'),
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('key', (string) $form_state->getValue('key'))
      ->set('url', (string) $form_state->getValue('url'))
      ->set('version', (string) $form_state->getValue('version'))
      ->set('account', (string) $form_state->getValue('account'))
      ->set('method', (string) $form_state->getValue('method'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
